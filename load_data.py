"""Load initial data from files"""
import cPickle as pickle
from numpy import load

def load_ge():
    data = load('yeast_gene_expr.npz')
    E = data['matrix_E']
    genes = data['genes']
    experiments = data['experiments'][2:]
    rapamycin = load('rapamycin.npy')
    rapamycin.shape=(rapamycin.shape[0],1)
    return (E,genes,experiments,rapamycin)

def load_GO():
    # Load GO adj matrix
    with open('GO_adj_matrix.dat', 'rb') as infile:
        A = pickle.load(infile)
    return(A)
            
def load_factors():
    Wp,Hp,Wn,Hn = load('factors.npy')
    with open('factor_fit.dat', 'rb') as outfile:
        fit = pickle.load(outfile)
    return(Wp,Hp,Wn,Hn,fit)

def load_ge_proc():
    ge_proc = load('ge_proc_matrix')
    return ge_proc
