import nimfa
import lsqlin
from numpy import where,array,zeros
from numpy.linalg import norm,lstsq
from numpy.random import randint
from math import sqrt

def splitpm(A):
        """Function splitpm(A) splits a matrix A into 
        positive and negative parts A = A_+ - A_-
        """
        Ap = A.copy()
        Am = -A.copy()
        Ap[where(Ap<0)] = 0
        Am[where(Am<0)] = 0
        return (Ap,Am)

def nmf(E,k):
    """Performs NMF on positive and negative parts of a matrix"""
    Ep,Em = splitpm(E)
    nmf_func = lambda Ep,k: nimfa.mf(Ep, 
            seed = "random_c", 
            rank = k, 
            method = "lsnmf", 
            max_iter = 12,
            n_run = 5,
            initialize_only = True,
            update = 'euclidean',
            objective = 'fro')
    fct_p = nmf_func(Ep,k)
    fit_p = nimfa.mf_run(fct_p)
    fct_n = nmf_func(Em,k)
    fit_n = nimfa.mf_run(fct_n)
    return(fit_p.basis(), fit_p.coef(), fit_n.basis(), fit_n.coef(), (fit_p,fit_n))

def query(q,basis):
    """Return xp, xn >=0, such that qp = Wp*xp and qm = Wm*xm"""
    qp,qm = splitpm(q)
    Wp,Wm = basis
    xp_r = lsqlin.lsqlin(Wp, qp, reg=0, lb=0, opts={'show_progress': False})
    xm_r = lsqlin.lsqlin(Wm, qm, reg=0, lb=0, opts={'show_progress': False})
    xp = array(xp_r['x'])
    xm = array(xm_r['x'])
    return (xp,xm)

def stdnorm(W,H):
    """Normalize W and H, so that the collumns of W have comparable size"""
    d,k = W.shape
    for i in range(k):
        f = sqrt(d)/norm(W[:,i],2)
        W[:,i] = W[:,i]*f
        H[i,:] = H[i,:]/f
    
def sel_by_zscore(x,p=0.95,k=5):
    from scipy.stats import norm as dnorm
    """Select k features, that have the highest z-score"""
    za = dnorm.ppf(p)
    zscore =  x/norm(x,2)*sqrt(len(x))
    return list(where(zscore > za)[0])

def zscores(X):
    """Calculate z-scores by collumns"""
    n,m = X.shape
    zs = X.copy()
    #zscore = lambda x: x/norm(x,2)*sqrt(len(x))
    for i in range(m):
        zs[:,i] = zs[:,i]/norm(zs[:,i],2)*sqrt(n)
    return zs

def nmf_ort(X,k):
    """Calculates orthogonal NMF for a matrix X"""
    n,m = X.shape
    F = zeros((n,k))
    G = zeros((m,k))
    # random vcolumns of E
    F = X[:,randint(m,size=k)].copy()
    # update rules
    for i in range(12):
        XTF = X.T.dot(F)
        XG = X.dot(G)
        FTF = F.T.dot(F)
        G = lstsq(F,X)[0].T
        F = F*XG/(F.dot(F.T.dot(XG)))
    return(F,G.T)
