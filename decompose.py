"""Kombinatorial part of gene expression querry"""
from IPython.display import HTML
import pandas as pd
from numpy import *
import matplotlib.pyplot as plt

def print_decomposition(q,index,M,E,genes,experiments):
    """Pretty prints decomposition of q"""
    idx = []
    for i in range(len(index)):
        idx += M[i]
    data = q[idx]
    cov = len(idx)
    plt.figure()
    k = len(index)
    bw = 0.25/(2.0+1.5*k)
    lrw = 0.4/(2.0+1.5*k)
    w = (1.0-2*lrw-bw*k)/float(k+1)
    vmax = abs(q).max()
    vmin = -vmax
    extent = [lrw,1,0.05,0.9]
    plt.subplot(1,k+1,1)
    plt.imshow(q[idx], interpolation='nearest',origin='upper',extent=extent, vmin=vmin,vmax=vmax)
    plt.xticks([])
    plt.yticks([])
    for i in range(len(index)):
        qp = zeros_like(q)
        qp[M[i],0]=E[M[i],index[i]]
        plt.subplot(1,k+1,i+2)
        plt.imshow(qp[idx], interpolation='nearest',origin='upper',extent=extent,vmin=vmin,vmax=vmax)
        plt.xticks([])
        plt.yticks([])
        data = hstack((data,qp[idx]))
    plt.figure()
    plt.imshow(data[:,:], cmap='RdYlGn', interpolation='nearest',origin='upper', extent=[0,1,0,2],vmin=vmin,vmax=vmax)
    plt.xticks([])
    plt.yticks([])
    data = hstack((genes[idx],data))
    data = pd.DataFrame(data,columns=['gene','query']+ list(experiments[index]))
    plt.show()
    return HTML(data.to_html())
    
def score(q,idx,M,E):
    """Calculate the score of (idx,M,E) representation of q.
    returns Pearson correlation coef, gene coverage and euclidian distance"""
    n = len(q)
    qp = zeros_like(q)
    cov = 0
    for i in range(len(idx)):
        qp[M[i],0] += E[M[i],idx[i]]
        cov += len(M[i])
    # Euclidian distance
    d = linalg.norm(qp-q)
    # correlation
    n = len(q)
    Ex = sum(q)/n
    Ey = sum(qp)/n
    Exy = q.T.dot(qp)[0,0]
    sx = sqrt(sum(q**2)-Ex**2)
    sy = sqrt(sum(qp**2)-Ey**2)
    r = (Exy-Ex*Ey)/sx/sy
    return r,cov,d
# find optimal decomposition of q from selected vectors of E
def decompose(q,selection,E,treshold):
    """Decompose the vector q to parts of selected vectors from E"""
    #mq = where(abs(q)>treshold)[0]
    M = []
    eis = E[:,selection] #[mq,:]
    dif = q.dot(ones((1,len(selection))))
    dif = abs(dif-eis)/dif # relative error
    mindif = amin(dif,1)
    mini = argmin(dif,1)
    for i in range(len(selection)):
        ei = eis[:,i]
        M.append(where((abs(ei)>treshold) 
            & (mini == i) 
            & (mindif<0.5))[0].tolist())
    return M

from itertools import product
from heapq import heapreplace, nlargest,heappush
# find optimal decomposition by brute force
def exhaustive_search(selection_list,q,E,treshold=2,maxn=100):
    """Perform exhaustive search through all of the combinations of collumns of E to find the one that 
    approximates q the best"""
    pri_queue = []
    n = 0
    for selection in product(*selection_list):
        M = decompose(q,selection,E,treshold)
        r,cov,d = score(q,selection,M,E) #correlation, coverage, distance
        item = (r,(selection,M))
        n += 1
        # if queue is full
        if n> maxn:
            if item > pri_queue[0]:
                heapreplace(pri_queue,item)
        else:
            heappush(pri_queue, item)
    # sort the priority queue
    return nlargest(maxn,pri_queue)
