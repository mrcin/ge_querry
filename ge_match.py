from numpy import nonzero
from graph_tool.all import boykov_kolmogorov_max_flow, push_relabel_max_flow,min_st_cut


def ge_match(q,e,graph,params):
        """
        The function (genes,score) = ge_match(q,e) finds similarities in two GE measurements q and e.
        It tries to find clusters of genes that are connected in GO.
        q,e ... two GE measurements
        genes ... the list of indexes corresponding to similar genes
        score ... the score of how similar are the measurements (the less, the better) TODO (0-100%)
        params ... parameters lambda and eta
        go_data ... lists of genes, processes and adjacency matrix
        """
        (l,eta) = params
        graph.set_directed(True)
        # fill current connections with eta capacity
        capacity = graph.new_edge_property("double")
        capacity.get_array()[:] = l
        # calculate which vertices go from source to target
        dif = (q-e)**2-eta
        s = graph.add_vertex() # source
        t = graph.add_vertex() # target 
        to_s = nonzero(dif>0)[0] # vertices linked to source
        to_t = nonzero(dif<0)[0] # vertices linked to target
        for i in to_s:
            e = graph.add_edge(s,graph.vertex(i))
            capacity[e] = dif[i]
            e = graph.add_edge(s,graph.vertex(i))
            capacity[e] = dif[i]
        for i in to_t:
            e = graph.add_edge(t,graph.vertex(i))
            capacity[e] = -dif[i]
            e = graph.add_edge(graph.vertex(i),t)
            capacity[e] = -dif[i]
        # calculate the min st cut
        residual = boykov_kolmogorov_max_flow(graph,s,t,capacity) # faster than push relabel
        #residual = push_relabel_max_flow(graph,s,t,capacity)
        (min_cut,partition) = min_st_cut(graph,s,residual)
        # clean up the augmented source and target
        graph.remove_vertex(t)
        graph.remove_vertex(s)
        return (min_cut,partition)
