# Matrix factorisations and decomposietions
import lsqlin
import nimfa

def splitpm(A):
        """Function splitpm(A) splits a matrix A into positive and negative parts
        A = A_+ - A_-
        """
        Ap = A.copy()
        Am = -A.copy()
        Ap[where(Ap<0)] = 0
        Am[where(Am<0)] = 0
        return (Ap,Am)
def mfpm(E,k):
    """ Factorize positive and negtive parts of E=Ep-Em, Ep,E>=0 into 
    nonegative factors Ep=Wp.dot(Hp) and Em=Wm.dot(Hm)"""
    Ep, Em = splitpm(E)
    fctr = nimfa.mf(Ep, seed = "random_c", rank = k, method = "nmf", max_iter = 12,
            initialize_only = True, update = 'euclidean',objective = 'fro')
    fit = nimfa.mf_run(fctr)
    Wp = fit.basis()
    Hp = fit.coef()
    print "Distance ", fit.distance()
    fctr = nimfa.mf(Erm, seed = "random_c", rank = k, method = "nmf", max_iter = 12,
            initialize_only = True, update = 'euclidean',objective = 'fro')
    fit = nimfa.mf_run(fctr)
    Wm = fit.basis()
    Hm = fit.coef()
    print "Distance ", fit.distance()
    return (Wp,Wm,Hp,Hm)

def query(q,Wp,Wm):
    """ Decompose the positive and negative parts of the vector q=qp-qm, qp,qm>=0
    as qp = Wp.dot(xp) and qm = Wm.dot(xm)"""
    qp,qm = splitpm(q)
    d,k = Wp.shape
    xp_r = lsqlin.lsqlin(Wp,qp,reg=0,lb=0,opts={'show_progress': False})
    xm_r = lsqlin.lsqlin(Wm,qm,reg=0,lb=0,opts={'show_progress': False})
    xp = array(xp_r['x'])
    xm = array(xm_r['x'])
    return (xp,xm)
